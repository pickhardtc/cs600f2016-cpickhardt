#!/usr/bin/env python

DATE = "24 Janurary 2017"
VERSION = "4"
AUTHOR = "Oliver Bonham-Carter"
AUTHOREMAIL = "obonhamcarter@allegheny.edu"


######################################################################
#note: good general ref: http://networkx.lanl.gov/tutorial/tutorial.html
# Much thanks to Ishwor Thapa for his help in getting this code working.

# for graphing the domains
# for x in `ls pdom*`; do ../../toGraph3_ii.py $x e; done

# for x in `ls *`; do ../toGraph2_iv.py $x b; done
# run as batch in bash:
# for x in `ls *asite*`; do ~/Desktop/toGraph2_iv.py $x s; done
#
# or for query results (non-cliques)
# for x in `ls *.txt`; do ~/Desktop/toGraph2_iv.py $x q; done
# for x in `ls *clique*`; do ~/Desktop/toGraph2_iv.py $x m; done

# necessary libraries to graph in python. run the two
# apt-get commands below to install.
# sudo apt-get install python-matplotlib
# sudo apt-get install python-networkx

#mac OS installation of libraries

#Installing the networkx libraries on Mac Yosemite
#(After having installed xcode from the app store on the mac desktop)

#sudo easy_install networkx
#sudo easy_install matplotlib

#if needed for dependancies, install pip
#sudo easy_install pip
######################################################################

# make a comparative domain size box plot

#(mito)
#../../../toGraph2_iv.py "pdom_inDomain_arabidopsis_thaliana_mito_glycosylation_domLengths.txt pdom_inDomain_homo_sapiens_mito_glycosylation_domLengths.txt pdom_inDomain_mus_musculus_mito_glycosylation_domLengths.txt pdom_inDomain_rattus_norvegicus_mito_glycosylation_domLengths.txt" x

#(non_mito)
#../toGraph2_iv.py "pdom_inDomain_arabidopsis_thaliana_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_caenorhabditis_elegans_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_canis_familiaris_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_danio_rerio_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_homo_sapiens_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_mus_musculus_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_oryctolagus_cuniculus_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_rattus_norvegicus_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_saccharomyces_cerevisiae_nonMito_glycosylation_domLengths.txt
#pdom_inDomain_xenopus_laevis_nonMito_glycosylation_domLengths.txt
#" x




dataDir = "out/"

NODESANDEDGES = True # used to output a NodesAndEdges statistics file for the simple graph.
#NODESANDEDGES = False # do not output a NodesAndEdges statistics file.

ROLLING_BINS = True # used to super impose histograms of smaller bin sizes from histogram().
#ROLLING_BINS = False
NUMBER_OF_BINS_V = 30 # Used for the superimposed histograms in histogram()

NUMBER_OF_BINS = 50 # used for the histogram (histogram()) method. The bins is the number of sum intervals
	  # for plotting magnitudes.
#NUMBER_OF_BINS = 20 # Used for histogram1()

REMOVE_DATA_FILE = True #Do not remove the parsed input files.
#REMOVE_DATA_FILE = False  #Remove the parsed input files.

INCLUDE_EDGE_LABELS = True # Display the labels.
#INCLUDE_EDGE_LABELS = False # Do not display the labels.

ADD_TITLE = True #  Add the title to the graphic
#ADD_TITLE = False # Do not add the title to the graphic

#MAKE_GRAPH = True # Plot the network to the screen?
MAKE_GRAPH = False # Do not plot the network to the screen?

NODE_SIZE = 2000 # Node size
#was 300

MIN_NODE_SIZE = 500 # 's' option node min limit

LINE_WIDTHS = 0 # size of the lines drawing the nodes
# was =1
#FontSize = 16 # option 'm' size of node titles
FONTSIZE = 16 # option 'm' size of node titles

def help():
	print "\n   ",DATE,"| version:",VERSION," |",AUTHOR,"|",AUTHOREMAIL
        print "  -------------------------------------------------------------------------------------------"

        print """

        Program to parse the "asite" files from getPTM3_ii.py
	and make them into graphs. Uses Network X
        perform queries over a uniprot.db file.

	Be sure to install the libraries.
	$ sudo apt-get install python-matplotlib
	$ sudo apt-get install python-networkx

	Arguments:
	<s> Simple graphs where two nodes are connected by
		a single edge (asitedata).
	<q> Two nodes: query data: two variables.
	<m> Three nodes: query data: three variables (cliques).
	<h> Heatmaps of matrix data. See source code for file format.
		must have installed R and the pheatmap library
		> install.packages(pheatmap)
	<b> Plots a Bar chart of data from domain project outputs.
	<p> Plots a Bar chart from a list of numbers in file.
	<x> Plots Box plots.
	note: all file inputs must be in quotes.
	example : ./toGraph2_iv.py x "1.txt 2.txt 3.txt 4.txt"
	and note that the first line of each pdom file must have title
		for legend

 Note: to make AA frequency heatmaps:
	Use files:
		Acanthamoeba_castellanii_stats_reactiveSiteComposition_non-mito.txt
		Acanthamoeba_castellanii_stats_AaComposition_non-mito.txt

		$ cat *_reactiveSiteComposition* | sort | uniq > non-mito_reactiveSiteComposition.txt

	Be sure to remove the redundant "organism" lines" and place the
	remaining one at the top of the file in
	non-mito_reactiveSiteComposition.txt
	run: $./toGraph2_iv.py non-mito_reactiveSiteComposition.txt h

 Note: to make PTM frequency heatmaps:
	Use getPTMfreqs to parse the data and then make a matrix
	file for toGraph2_iv.py

		$ cat *ptmFr* | sort | uniq > 000all_nonmito_PTMfreqs.txt
		$ ./getPTMfreqs_iv.py 000all_nonmito_PTMfreqs.txt p
		$ ./toGraph2_iv.py all_nonmito_PTMfreqs_hmData.txt h
		$ ../../toGraph2_iv.py 000all_nonmito_PTMfreqs_hmData.txt h

        usage: programName <s,q,m,h,b,p,x,y,c,e> <dataFile.txt>

		Use option:
			'h' for heatmaps
			'e' for domain and PTM locational plotting
			'y' for boxplots with titles
			'b' for before, inside and after domain ptm plotting

"""
#	update:
#		now dynamic edge and node sizes with 's' option
#		linear graphs are used for 30 or less nodes.
#		Now does heatmaps for data in x*y matrix where
#		top and left are labeled.

	print "        Note: How to make a rolling-bin histogram:"
	print "         Adjust the number of super-imposed bins to use in code"
	print "         by setting the variables, ROLLING_BINS = True"
	print "         and setting NUMBER_OF_BINS_V to some value (currently =",NUMBER_OF_BINS_V,")."
	print " for x in `ls *`; do ../toGraph2_iv.py $x b; done"

	print "\n        Graphing to the screen : ",MAKE_GRAPH
        print "        Output_dir: ",dataDir, "\n"


#	<o> Three nodes: query data (original method)
#	<w> Three nodes: query data. (working method)

# end of help()

def graphDemo(inFile1):

	print "  Graphing demo."
	print """
	Draw a graph with matplotlib, color by degree.

	You must have matplotlib for this to work.
	"""
#	__author__ = """Aric Hagberg (hagberg@lanl.gov)"""
#	import matplotlib.pyplot as plt
#	import networkx as nx

	G=nx.cubical_graph()
	pos=nx.spring_layout(G) # positions for all nodes

	# nodes
	nx.draw_networkx_nodes(G,pos,
                       nodelist=[0,1,2,3],
                       node_color='r',
                       node_size=500,
		       alpha=0.8)
	nx.draw_networkx_nodes(G,pos,
                       nodelist=[4,5,6,7],
                       node_color='b',
                       node_size=500,
		       alpha=0.8)

	# edges
	nx.draw_networkx_edges(G,pos,width=1.0,alpha=0.5)
	nx.draw_networkx_edges(G,pos,
                       edgelist=[(0,1),(1,2),(2,3),(3,0)],
                       width=8,alpha=0.5,edge_color='r') # red
	nx.draw_networkx_edges(G,pos,
                       edgelist=[(4,5),(5,6),(6,7),(7,4)],
                       width=8,alpha=0.5,edge_color='b') # blue

	# some math labels
	labels={}
	labels[0]=r'$a$'
	labels[1]=r'$b$'
	labels[2]=r'$c$'
	labels[3]=r'$d$'
	labels[4]=r'$e$' #=r'$\alpha$'
	labels[5]=r'$f$' #=r'$\beta$'
	labels[6]=r'$g$' #=r'$\gamma$'
	labels[7]=r'$h$' #=r'$\delta$'

	#nx.draw_networkx_labels(G,pos,labels,font_size=16)
	nx.draw_networkx_labels(G,pos,labels,font_size=FONTSIZE)

	plt.axis('off')
	fname = dataDir + inFile1 + ".png"
	plt.savefig(fname) # save as png
	if MAKE_GRAPH == True:
		plt.show() # display
#end of graphDemo()


def checkDataDir():
#function to determine whether a data output directory exists.
#if the directory doesn't exist, then it is created

        try:
                os.makedirs(dataDir)
                print "  PROBLEM: output_dir doesn't exist"
                print "  * Creating :",dataDir
                return 1

        except OSError:
                return 0
#end of checkDataDir()

def printer(in_dic):
# function to print out contents of the dic or list.
        print "printer : data type is ", type(in_dic)
	print " Unformatted :",str(in_dic)
        counter = 0
        for i in in_dic:
#               print "  ",counter,"  key = ",i[:30],", value = ",in_dic[i][:30]
                print "  ",counter,"  key =",i,"  value = ",in_dic[i]
                counter = counter + 1
        return ""
#end of printer()


def openFile(file):
# read a list, return a dic
        try: #is the file there??
                data = open(file, "r").readlines() #makes a list
                return data
        except IOError:
                print "  \aNo such file!!!! \"",file,"\" so exiting"
                sys.exit(1)
#end of openFile()



def twoNodeGrapher(inFile1):
# method to graph nodes. Nodes on the right are red, left are blue.
# parsing must happen first to get these nodes in the right position.
#code taken from:
#http://stackoverflow.com/questions/16416307/networkx-encounter-node-has-no-position?rq=1
#http://stackoverflow.com/questions/8082420/networkx-change-node-color-in-draw-circular
#import matplotlib.pyplot as plt
#import networkx as nx

#	print "  simpleGrapher_twoNotes()"
#	print "  Graphing a query with two nodes (asite file from getPTM3_iii.py)."

	data_list = openFile(inFile1)
	inFile1 = inFile1.replace(".txt","")

        G=nx.Graph()

#parsing

        data1_nodes = set() #left side nodes (ptms)
        data2_nodes = set() # right side nodes (residues)
	edges_set = set()

	freqActiveSiteInSeq_dic = {} #key = node, holds AA freq data for org proteome
	freqPtm_dic = {} #key = node

	edgeWidth_dic = {} # edge sizes: key is residue, value = freqptm * freqactivesite
#org:ptm:activeSite:freqPTM:freqActiveSite:freqActiveSiteInSeq:ProductOfFreq
#Danio rerio:phosphorylation:E:0.146827477714:0.00943890928159:0.0703083455939:9.74397204111e-05

	for d in range(len(data_list)):
		try:
			d = data_list[d]
			#d = d.split(":")
			d = d.split("\t")
			#print d#d[1],"and ",d[2]
			#print d[0]
			if d[0] != "org": # some inputs have col headers in thhe first line.
				#d[2] = d[2].replace("interchain.*","interchain")
				d[1] = re.sub("interchain.*","interchain",d[1])
				data1_nodes.add(d[1]) # ptms
				data2_nodes.add(d[2]) # residues
				tmp = d[1],d[2] # this tuple is the connection between the nodes
				edges_set.add(tmp) # build the edges

				#freqActiveSite_dic[d[2]]  = d[4] # freqActiveSite for the residues
				freqActiveSiteInSeq_dic[d[2]]  = d[5] # freqActiveSite for the residues.
									# find freq of AA across whole proteome

				freqPtm_dic[d[1]] = d[3]
				edgeWidth_dic[d[2]] = float(d[3]) * float(d[4])
				#print "reactive site :",d, "fifth :", d[5]

						# freqActiveSiteInSeq for the residues
		except IndexError:
			pass

#	print " number of nodes :", G.number_of_nodes()
#	print " number of edges :", G.number_of_edges()

#add the nodes to the network
	G.add_nodes_from(data1_nodes)
	G.add_nodes_from(data2_nodes)
	G.add_edges_from(edges_set)

#	print " size of data1_nodes (PTMs) :",len(data1_nodes)
#	print " size of data2_nodes (Residues) :",len(data2_nodes)
#	print " after adding nodes, number of nodes :", G.number_of_nodes()
#	print " after adding nodes, number of edges :", G.number_of_edges()

# add the edges to the network

	position_dic = getPositions(data1_nodes,data2_nodes) # get positions for nodes and edges
						# note small data (less than ten nodes) is linear
						# otherwise graph is circular

	#colours1_list,colours2_list  = getColourList(0.3,0.5,0.7,0.9)
	#colours1_list,colours2_list  = getColourList(0.3,0.5,0.7,0.85)
	colours_dic = {1:'#DB6034',2:'#50BCDB',3:'#D3DB50'}

# The shape of the node: 'so^>v<dph8' (default='o').

# ref node drawing: http://networkx.lanl.gov/reference/generated/networkx.drawing.nx_pylab.draw_networkx_nodes.html

	if ADD_TITLE == True:
		plt.title("Title: " + str(inFile1))

#        print "freqActiveSite_dic =", freqActiveSite_dic
#        print "freqActiveSiteInSeq_dic=",freqActiveSiteInSeq_dic
#        print "data1_nodes = ",data1_nodes
#        print "data2_nodes = ",data2_nodes

	ptmNodes_list = [] #nodes for ptms
	freqPtm_list = [] #ptm freqs from freqPTM_dic (freq holder)
	ptmNsize_list = [] # node sizes

	residNodes_list = [] #node sizes for data2list nodes
	residNsize_list = [] #freq list for data2 (freq holder)



	edgeWidth_list = [] # holds the edge widths in order of data2_nodes (residues)
	for i in data2_nodes:
		residNodes_list.append(i)
		p = freqActiveSiteInSeq_dic[i]
		p = float(p)

#scale the node sizes up.
#		print "p = ", p
		p = p * 7000 #scaler
#		print  i," data2  p = ",p,type(p)

# this might work another time...
#		if p < 500: p = 500
#		if p > 1000: p = 1000

		residNsize_list.append(int(p))
		e = edgeWidth_dic[i]
		e = float(e) * 1000 #scaler
#		print i, e

# scale the edge thickness so that they are not too big, nor too small.
		if e < .9: e = 1
		if e > 3:  e = 4

		edgeWidth_list.append(e)

#	print "residNodes_list = ",residNodes_list
#	print "residNsize_list = ",residNsize_list


# find max size of freq
#        max1 = 0
#        for i in freqPtm_dic:
#                #print "i =",i, freqActiveSite_dic[i]
#                if max1 < freqPtm_dic[i]:
#                        max1 = freqPtm_dic[i]
#        max1 = float(max1) * 40


        for i in data1_nodes:
                ptmNodes_list.append(i)
                p = freqPtm_dic[i]
                p = float(p)
                p = p * 7000 #scaler
#               print i," data1  p = ",p,type(p)

#                if p < MIN_NODE_SIZE: p = MIN_NODE_SIZE
#                if p > 1000: p = 1000

                ptmNsize_list.append(int(p))


        nx.draw_networkx_nodes(G,position_dic,
        nodelist=data1_nodes, node_color=colours_dic[1],
        node_size = ptmNsize_list,#NODE_SIZE,
        node_shape = '<',
        linewidths = LINE_WIDTHS) # ptms

        nx.draw_networkx_nodes(G,position_dic,
        nodelist=residNodes_list, node_color=colours_dic[2],
        node_size = residNsize_list,
	node_shape = 'o',
        linewidth = LINE_WIDTHS) # residues

	#print "edge_list :",edge_list,type(edge_list)

	nx.draw_networkx_edges(G,position_dic,edge_color='g',width = edgeWidth_list)
# edge_list was 0.5 then 1.5, add edge positions
	if INCLUDE_EDGE_LABELS == True:
		#nx.draw_networkx_labels(G,position_dic,font_size = 18) #was 10 # add edges labels
		nx.draw_networkx_labels(G,position_dic,font_size = FONTSIZE) #was 10 # add edges labels


#	print " Filename : " ,inFile1
	org = inFile1[:inFile1.find("_asite")] # guess the name of the org from the filename up to the "_asite" part.

	print "\n Org: ",org
	print "  ------NODES and EDGES------ "
        print " 1.  Number of PTMs      :",len(data1_nodes)
        print " 2.  Number of Residues  :",len(data2_nodes)
        print " 3.  Total nodes         :", G.number_of_nodes()
        print " 4.  Number of Edges     :", G.number_of_edges()
	print "  ----------------------------"
	print "\n  Indexed Node Degree :",
	print nx.degree_centrality(G)
	print "\n"
	if NODESANDEDGES == True:
#save the node file
		tmp = "Org\tPTMs\tRSs\tEdges\tTotalNodes\n"
		tmp = tmp + str(org) + "\t"
		tmp = tmp + str(len(data1_nodes)) + "\t"
		tmp = tmp + str(len(data2_nodes)) + "\t"
		tmp = tmp + str(G.number_of_edges()) + "\t"
		tmp = tmp + str(G.number_of_nodes()) + "\n"
		fname = dataDir + inFile1 + "_NodesAndEdges.txt"
		f = open(fname,"w")
		f.write(tmp)
		f.close()
		print "  * NodesAndEdges file Saved :",fname

#degree (edges) file

		print "nx.degree_centrality(G) type :",type(nx.degree_centrality(G))
		degree_dic = nx.degree_centrality(G)

		header = "Org\tPTM\tDegree\n"
		tmp1 = tmp2 = ""
		for d in degree_dic:
			if len(str(d)) > 1 : # is ptm
				tmp1 = tmp1 + str(org) + "\t"
				tmp1 = tmp1 + str(d)   + "\t"
				tmp1 = tmp1 + str(degree_dic[d]) + "\n"
			else:
      				tmp2 = tmp2 + str(org) + "\t"
				tmp2 = tmp2 + str(d)   + "\t"
				tmp2 = tmp2 + str(degree_dic[d]) + "\n"

		tmp = header + tmp1 + tmp2
	        fname = dataDir + inFile1 + "_edgeDegrees.txt"
                f = open(fname,"w")
                f.write(tmp)
                f.close()
		print "  * Edges degrees file Saved :",fname


        plt.axis('off')
	fname = dataDir + inFile1 + ".png"
        plt.savefig(fname, bbox_inches=0)
	print "  * File Saved :",fname

        if MAKE_GRAPH == True:
                plt.show() # display
	#del fig
#end of twoNodeGrapher()



def query_twoNodeGrapher(inFile1):
## EDIT THIS METHOD!!

# method to graph nodes. Nodes on the right are red, left are blue.
# parsing must happen first to get these nodes in the right position.
#code taken from:
#http://stackoverflow.com/questions/16416307/networkx-encounter-node-has-no-position?rq=1
#http://stackoverflow.com/questions/8082420/networkx-change-node-color-in-draw-circular
#import matplotlib.pyplot as plt
#import networkx as nx

	print "  query_twoNodeGrapher()"
	print "  Graphing query data having only two nodes."
###
        #must clean the data a bit...
        data_list = open(inFile1,"r").read()
#       print data_list, type(data_list)
        data_list = data_list.replace("|"," ")
        inFile1 = inFile1.replace(".txt","")
        fname = dataDir  + inFile1 + "_dat.txt"
        f = open(fname,"w")
        f.write(data_list)
        f.close()

        data_list = openFile(fname)
        if REMOVE_DATA_FILE == True:
                os.remove(fname)

###


#	data_list = openFile(inFile1)
	inFile1 = inFile1.replace(".txt","")

        G=nx.Graph()

#parsing

        data1Nodes_set = set() #left side nodes
        data2Nodes_set = set() # right side nodes
	edges_set = set()
	for d in range(len(data_list)):
		line = data_list[d]
		try:
			line = string.split(line)
			#print "line = ",line
			firstNode = line[0]
			secondNode = line[1]
			edge = firstNode,secondNode
			#print "f,s, edge =",firstNode,secondNode,edge
			data1Nodes_set.add(firstNode)  # first thing
			data2Nodes_set.add(secondNode) # second thing
			edges_set.add(edge) # this edge
		except IndexError:
			pass
#	print " number of nodes :", G.number_of_nodes()
#	print " number of edges :", G.number_of_edges()


#add the nodes to the network
	G.add_nodes_from(data1Nodes_set)
	G.add_nodes_from(data2Nodes_set)
	G.add_edges_from(edges_set)

#	print " size of data1_nodes (PTMs) :",len(data1Nodes_set)
#	print " size of data2_nodes (Residues) :",len(data2Nodes_set)

#	print " after adding nodes, number of nodes :", G.number_of_nodes()
#	print " after adding nodes, number of edges :", G.number_of_edges()

# add the edges to the network


	print " after adding edges, number of nodes (PTMs and Residues ) :", G.number_of_nodes()
	print " after adding edges, number of edges (PTM to Residues) :", G.number_of_edges()

	position_dic = getPositions(data1Nodes_set,data2Nodes_set) # get positions for nodes and edges
						# note small data (less than ten nodes) is linear
						# otherwise graph is circular

	colours1_list,colours2_list  = getColourList(0.3,0.5,0.7,0.9)
	#colours1_list,colours2_list  = getColourList(0.3,0.5,0.7,0.85)

## draw each set of nodes in colours (violet-blues and yellow-greens)

# The shape of the node. Specification is as matplotlib.scatter marker, one of 'so^>v<dph8' (default='o').
# ref node drawing: http://networkx.lanl.gov/reference/generated/networkx.drawing.nx_pylab.draw_networkx_nodes.html

	if ADD_TITLE == True:
		plt.title("Title: " + str(inFile1))

	nx.draw_networkx_nodes(G,position_dic, nodelist=data1Nodes_set, node_color=colours1_list, node_size = 300, node_shape = '>',linewidths = LINE_WIDTHS) # ptms
	nx.draw_networkx_nodes(G,position_dic, nodelist=data2Nodes_set, node_color=colours2_list, node_size = 250,linewidths = LINE_WIDTHS) # residues


	nx.draw_networkx_edges(G,position_dic,edge_color='g',width = 0.5) # add edge positions
	if INCLUDE_EDGE_LABELS == True:
		#nx.draw_networkx_labels(G,position_dic,font_size=10) # add edges labels
		nx.draw_networkx_labels(G,position_dic,font_size=FONTSIZE) # add edges labels


        plt.axis('off')
	fname = dataDir + inFile1 + ".png"
        plt.savefig(fname)
	print "  * File Saved :",fname
        if MAKE_GRAPH == True:
                plt.show() # display
	#del fig
#end of query_twoNodeGrapher()




def getColourList(s1,e1,s2,e2):
# method to return a colour designing list. The inputs are the ranges
# for the colour scheme.
# the method returns two lists. The first is for lighter colours and the second is
# for darker colours.

# draw each set of nodes in colours (violet-blues and yellow-greens)
#yellows
#        s1 = 0.3 # start of colour range
#        e1 = 0.5 # end of colour range

#blues
#        s2 = 0.7 # start of colour range
#        e2 = 0.9 # end of colour range

	colours1_list = [(random.uniform(s2,e2), random.uniform(s2,e2), random.uniform(s1,e1)) for i in range(5)]
	colours2_list = [(random.uniform(s1,e1), random.uniform(s1,e1), random.uniform(s2,e2)) for i in range(5)]

	return colours1_list, colours2_list # lighter list and darker colour list.
#end of getColoursList()


def getPositions(data1_nodes,data2_nodes):
#function to get positions to plot this mess
# if a datasets has less than or equal to ten elements, then it is linear, otherwise circular
# first establish the positioning system of all

        # note: the positioning system is the following for the screen
        # top       = (x,y) -> (0.5,1)
        # bottom    = (x,y) -> (0.5,0)
        # left      = (x,y) -> (0,0.5)
        # right     = (x,y) -> (1,0.5)


        tmp1 = [] # position of a node [x,y]
        position1_dic = {} # holds positions of first node group
        position2_dic = {}

        if len(data2_nodes) >= len(data1_nodes):
                maxSize = len(data2_nodes)
        else:
                maxSize = len(data1_nodes)

#linear display for small data

        if len(data1_nodes) <= 40 or len(data2_nodes) <= 40:

                x = 0
                y = 0
                for i in data1_nodes: #ptms
        #               print i, ";; x =",x, "y = ",y
                        #y = y + 1.0/ (len(data1_nodes) + (len(data1_nodes)/2)) # incrementally running up page
                        y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        tmp1 = [x, y]
                        position1_dic[i] = tmp1
        #       printer(position1_dic)
                x = 1.7
                y = 0
                for i in data2_nodes: #residues
        #                print i, ";; x =",x, "y = ",y
                        #y = y + 1.0 / (len(data2_nodes) + (len(data2_nodes)/2)) # incrementally running up page
                        y = y + len(data1_nodes) / (maxSize*1.0) # incrementally running up page
                        tmp2 = [x, y]
                        position2_dic[i] = tmp2
        #       printer(position1_dic)


# circular display for large data
        else:
		theta = 0
		scale_factor = 2
                degree = len(data1_nodes)
                for i in data1_nodes: #ptms
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        theta_incremental = 2 * math.pi / degree

                        y = math.sin(theta)
			#for the nodes from 90 to 270 (left), place it little higher than right nodes
			if theta > (math.pi/2.) and theta < (3*math.pi/2.): # add some spacing on left so that edge names are not jumbled
				y = y + 0.04
                        x = math.cos(theta) - 2

			#scaling the left figure by a scale factor
			x = x * scale_factor
			y = y * scale_factor

        #               print degree, theta, i, ";; x =",x, "y = ",y
                        tmp1 = [x, y]

                        #degree = degree + degree
			theta = theta + theta_incremental
                        position1_dic[i] = tmp1
        #       printer(position1_dic)

		theta = 0
                degree = len(data2_nodes)
                for i in data2_nodes: #ptms
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        theta_incremental = 2 * math.pi / degree

                        y = math.sin(theta)
                        x = math.cos(theta) + 2

        #                print degree, theta, i, ";; x =",x, "y = ",y
                        tmp2 = [x, y]

                        theta = theta + theta_incremental
                        position2_dic[i] = tmp2
#        printer(position2_dic)


        # add both dics together
        position_dic = {} # holds both dics
        for i in position1_dic:
                position_dic[i] = position1_dic[i]

        for i in position2_dic:
                position_dic[i] = position2_dic[i]

	return position_dic
#end of getPositions()


def query_ThreeNodeGrapher(inFile1):
#
# A method to graph data where it is hashable
# input data looks like:
# SYM1 STRESS1 PTM1 PMID1
# SYM2 STRESS2 PTM2 PMID2
# SYM1 STRESS2 PTM1 PMID2

        print "query_ThreeNodeGrapher()"
	print "  Graphing a query with three nodes (a clique)."

        #must clean the data a bit...
        data_list = open(inFile1,"r").read()
#       print data_list, type(data_list)
        data_list = data_list.replace("|"," ")
        inFile1 = inFile1.replace(".txt","")
        fname = dataDir  + inFile1 + "_dat.txt"
        f = open(fname,"w")
        f.write(data_list)
        f.close()

        data_list = openFile(fname)
        if REMOVE_DATA_FILE == True:
                os.remove(fname)

	symNodes_dic = {} #holds the protein types
	stressNodes_dic = {} # holds the stress types
	ptmNodes_dic = {} #holds the ptm types


        G = nx.MultiGraph()
        for i in data_list:
                line_list = i
                line_list = line_list.split("|")
                for l in line_list:
                        ll_list = string.split(l)

#keep track of what node belongs to which data-type
			symNodes_dic[ll_list[0]] = "sym"
			stressNodes_dic[ll_list[1]] = "stress"
			ptmNodes_dic[ll_list[2]] = "ptm"

                        for i in range(0,len(ll_list)-1):
                                element = ll_list[i]
                                G.add_node(element)

                        edge_label = ll_list[len(ll_list)-1]

                        for i in range(0,len(ll_list)-2):
                                for j in range(i+1,len(ll_list)-1):
                                        tmp = ll_list[i],ll_list[j]
                                        G.add_edge(tmp[0],tmp[1],pmc=ll_list[3])

#        colours1_list,colours2_list  = getColourList(0.1,0.5,0.7,1) #note: list 1 is lighter colours, list 2 is darker
        if ADD_TITLE == True:
                plt.title("Title: " + str(inFile1))

#        nx.draw_networkx(G,pos=nx.spring_layout(G),with_labels=True,node_color=colours1_list,node_size = 500,edge_color='g',width = 0.5)

        print " Size of data (lines) :",len(data_list)
        print " Nodes:",G.number_of_nodes()
        print " Edges: ",G.number_of_edges()


# The node types come from these dics
	#printer(symNodes_dic)
	#printer(stressNodes_dic)
	#printer(ptmNodes_dic)


	colour_map = {'sym':'#DB6034','stress':'#50BCDB','ptm':'#D3DB50'}
	shape_map =  {'sym':'o','stress':'s','ptm':'p'}


# The shape of the node. Specification is as matplotlib.scatter marker, one of 'so^>v<dph8' (default='o').
# ref node drawing: http://networkx.lanl.gov/reference/generated/networkx.drawing.nx_pylab.draw_networkx_nodes.html


	symNodes_list = []
	for i in symNodes_dic:
		symNodes_list.append(i)
#	print "symNodes_list :",symNodes_list


	stressNodes_list = []
	for i in stressNodes_dic:
		stressNodes_list.append(i)
#	print "stressNodes_list :",stressNodes_list

	ptmNodes_list = []
	for i in ptmNodes_dic:
		ptmNodes_list.append(i)
#	print "ptmNodes_list :",ptmNodes_list

	nodeSet_dic = {}
	nodeSet_dic["sym"]    = symNodes_list
	nodeSet_dic["stress"] = stressNodes_list
	nodeSet_dic["ptm"]    = ptmNodes_list

#	pos=nx.spring_layout(G) # positions for all nodes

	position_dic = getThreePositions(symNodes_list,stressNodes_list,ptmNodes_list)
	pos = position_dic

#	print "position_dic"
#	printer(pos)

#	print "position_dic"

	node_list = ["ptm","stress","sym"]


	for thisNode in node_list:
		#thisNode = "ptm"
		nx.draw_networkx_nodes(G, pos,nodelist=nodeSet_dic[thisNode],
		node_color=colour_map[thisNode], node_size = NODE_SIZE, linewidths=LINE_WIDTHS,
		node_shape = shape_map[thisNode]) # proteins


#	thisNode = "ptm"
#	nx.draw_networkx_nodes(G, pos,nodelist=nodeSet_dic[thisNode],
#	node_color=colour_map[thisNode], node_size = NODE_SIZE, linewidths=LINE_WIDTHS,
#	node_shape = shape_map[thisNode]) # proteins


#	thisNode = "stress"
#	nx.draw_networkx_nodes(G, pos,nodelist=nodeSet_dic[thisNode],
#	node_color=colour_map[thisNode], node_size = NODE_SIZE, linewidths=LINE_WIDTHS,
#	node_shape = shape_map[thisNode]) # proteins

#	thisNode = "sym"
#	nx.draw_networkx_nodes(G, pos,nodelist=nodeSet_dic[thisNode],
#	node_color=colour_map[thisNode], node_size = NODE_SIZE, linewidths=LINE_WIDTHS,
#	node_shape = shape_map[thisNode]) # proteins


	nx.draw_networkx_edges(G,pos,edge_color='g',width = .9) # add edge positions

	if INCLUDE_EDGE_LABELS == True:
		#nx.draw_networkx_labels(G,pos,font_size=10) # add edges labels
		nx.draw_networkx_labels(G,pos,font_size=FONTSIZE) # add edges labels


        plt.axis('off')
        fname = dataDir + inFile1 + ".png"
        plt.savefig(fname, bbox_inches="tight")
        print "  * File Saved :",fname
        if MAKE_GRAPH == True:
                plt.show() # display


#changeColour Ref: http://stackoverflow.com/questions/24662006/python-networkx-graph-different-colored-nodes-using-two-lists
#end of query_ThreeNodeGrapher()




def getThreePositions(data1_nodes,data2_nodes,data3_nodes):
#function to get positions to plot this mess
# if a datasets has less than or equal to ten elements, then it is linear, otherwise circular
# first establish the positioning system of all

        # note: the positioning system is the following for the screen
        # top       = (x,y) -> (0.5,1)
        # bottom    = (x,y) -> (0.5,0)
        # left      = (x,y) -> (0,0.5)
        # right     = (x,y) -> (1,0.5)


        tmp1 = [] # position of a node [x,y]
        position1_dic = {} # holds positions of first node group
        position2_dic = {}
	position3_dic = {}


        if len(data2_nodes) >= len(data1_nodes):
                maxSize = len(data2_nodes)
        else:
                maxSize = len(data1_nodes)

	if len(data3_nodes) >= maxSize:
		maxSize = len(data3_nodes)

#linear display for small data

        if len(data1_nodes) <= 10 or len(data2_nodes) <= 10 or len(data3_nodes) < 10:

                x = 0
                y = 0
                for i in data1_nodes: #ptms
        #               print i, ";; x =",x, "y = ",y
                        #y = y + 1.0/ (len(data1_nodes) + (len(data1_nodes)/2)) # incrementally running up page
                        y = y + NODE_SIZE # incrementally running up page
                        #y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        tmp1 = [x, y]
                        position1_dic[i] = tmp1
#                printer(position1_dic)
#		print "position1_dic"

                x = 0.5
                y = 0
                for i in data2_nodes: #residues
        #                print i, ";; x =",x, "y = ",y
                        #y = y + 1.0 / (len(data2_nodes) + (len(data2_nodes)/2)) # incrementally running up page
                        #y = y + len(data2_nodes) / (maxSize*1.0) # incrementally running up page
                        y = y + NODE_SIZE # incrementally running up page
                        tmp2 = [x, y]
                        position2_dic[i] = tmp2
#                printer(position2_dic)
#		print "position2_dic"

                x = -.5
                y = .2 # positive non zeros seems to move the thing down
                for i in data3_nodes: #residues
        #                print i, ";; x =",x, "y = ",y
                        #y = y + 1.0 / (len(data2_nodes) + (len(data2_nodes)/2)) # incrementally running up page
                        #y = y + len(data3_nodes) / (maxSize*1.0) # incrementally running up page
                        y = y + NODE_SIZE # incrementally running up page
                        tmp3 = [x, y]
                        position3_dic[i] = tmp3
#                printer(position3_dic)
#		print "position3_dic"



# circular display for large data
        else:
		theta = 0
		scale_factor = 2
                degree = len(data1_nodes)
                for i in data1_nodes: #ptms
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        theta_incremental = 2 * math.pi / degree

                        y = math.sin(theta)
			#for the nodes from 90 to 270 (left), place it little higher than right nodes
			if theta > (math.pi/2.) and theta < (3*math.pi/2.): # add some spacing on left so that edge names are not jumbled
				y = y + 0.04
                        x = math.cos(theta) - 2

			#scaling the left figure by a scale factor
			x = x * scale_factor
			y = y * scale_factor

        #               print degree, theta, i, ";; x =",x, "y = ",y
                        tmp1 = [x, y]

                        #degree = degree + degree
			theta = theta + theta_incremental
                        position1_dic[i] = tmp1
        #       printer(position1_dic)

		theta = 0
                degree = len(data2_nodes)
                for i in data2_nodes: #ptms
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        theta_incremental = 2 * math.pi / degree

                        y = math.sin(theta)
                        x = math.cos(theta) + 2

        #                print degree, theta, i, ";; x =",x, "y = ",y
                        tmp2 = [x, y]

                        theta = theta + theta_incremental
                        position2_dic[i] = tmp2

                theta = 0
                degree = len(data3_nodes)
                for i in data3_nodes: #ptms
        #               y = y + len(data2_nodes) / (maxSize*1.0)  # incrementally running up page
                        theta_incremental = 2 * math.pi / degree
                        y = math.sin(theta) -3
                        x = math.cos(theta)
                        tmp3 = [x, y]
                        theta = theta + theta_incremental
                        position3_dic[i] = tmp3

        # add both dics together
        position_dic = {} # holds both dics
        for i in position1_dic:
                position_dic[i] = position1_dic[i]

        for i in position2_dic:
                position_dic[i] = position2_dic[i]

        for i in position3_dic:
                position_dic[i] = position3_dic[i]


	return position_dic
#end of getThreePositions()


def dicToList(in_dic):
#converts a dic to a list (returns list)
	this_list = []
        for i in in_dic:
                this_list.append(i)
        print "  this_list :", this_list, type(this_list)
#end of dicToList()

def toHeatmap(inFile1):
# method to make rscript to graph a heatmap.
# data file must be the following:
#Organism	       :A:R:N:D:C:Q:E:G:H:I:L:K:M:F:P:S:T:W:Y:V
#Arabidopsis_thaliana  :1:2:3.4:5:6:0:0:7:8:0:0:9:3:0:4:5:0:0:8
#Caenorhabditis_elegans:1:2:3:4:5:6:0:0:7:8:0:0:9:2:0:4:5:0:0:8
#Aspergillus_fumigata  :0:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0:0

        print "  toHeatmap: using Rscript to make heatmaps of data."
	print "  Input file should be like the following:"
	print """
Organism              :A:R:N:D:C:Q:E:G:H:I:L:K:M:F:P:S:T:W:Y:V
Arabidopsis_thaliana  :1:2:3.4:5:6:0:0:7:8:0:0:9:3:0:4:5:0:0:8
Caenorhabditis_elegans:1:2:3:4:5:6:0:0:7:8:0:0:9:2:0:4:5:0:0:8
Aspergillus_fumigata  :0:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0:0
"""
#        data_list = openFile(inFile1)
#        inFile1 = inFile1.replace(".txt","")

	script1 = """
library(pheatmap)
warnings()
"""

	script2 = "dat <- read.table(\""+inFile1+"\", sep = \":\",header=T)"
	#script2 = "dat <- read.table(\""+inFile1+"\", sep = \"\\t\",header=T)"
#
        inFile1 = inFile1.replace(".txt","")
#
	fnamePNG = dataDir + inFile1 + ".png"

	script3 = """
row_length <-length(dat[,1])# nums of rows
col_length <-length(dat[1,]) #num of cols
dat[1:row_length,2:col_length] #the data portion
pal<-c(0)
for (x in 2:col_length)
{
        i<-as.numeric(dat[1:row_length,x])
        #print(i)
        pal<-c(pal,i)
}

dat_matrix<-matrix(pal[2:length(pal)],ncol=col_length-1,nrow=row_length, dimnames=list(dat[1:row_length,1],names(dat)[2:col_length]))
"""
#pheatmap(dat_matrix,filename="data.png",show_rownames=TRUE,show_colnames=TRUE)
	script4 = "pheatmap(dat_matrix,filename=\""+fnamePNG+"\",show_rownames=TRUE,show_colnames=TRUE)"
	script = script1 + script2 + script3 + script4

#	print " script is :",script

        inFile1 = inFile1.replace(".txt","")
	fnameR = dataDir + inFile1+".r"
	f = open(fnameR,"w")
	f.write(script)
	f.close()
	#print "   ** Saved Rscript :",fname
#must have r stat installed!
	command = "Rscript " + fnameR
	os.system(command)

	#cleanup
	command = "Rplots.pdf"
	os.remove(command)

	if REMOVE_DATA_FILE == True: #remove the rscript file too.
		os.remove(fnameR)
	print "   ** Saved png file :",fnamePNG
#end of toHeatmap()

def histogram(inFile1):

#	print "  Fancy Green Histograme Bar Chart."
#	print "  The data must look like the folloinwg:"
#	print "\n  Third column data to graph:"
#	print "    acetylation	p14882:5	138"
#	print "    acetylation	p14882:4	92"
#	print "    acetylation	q5xic0:0	12\n"

	data = openFile(inFile1)
	num_list = []
	title = ""
	for line in data:
		#print "line =",line
		if "mito" in string.lower(line):
		#if "domain" in string.lower(line):
			title = line
		else:
			sline = string.split(line)
			#print sline
			try:
				#num_list.append(int(sline[2]))
				num_list.append(float(sline[2]))
			except ValueError:

				print "sline[2] = ",sline[2], "is not an int..."
	print "  Data title :", title#,type(title)
	#print "num_list : ",num_list

	# make a bar chart

	#plt.hist(num_list,bins=50,normed = 1, facecolor='g',alpha=0.75)
	if ROLLING_BINS == True: #super impose graphs of smaller bins?
		for b in range(0,NUMBER_OF_BINS_V-1,2):
#			print NUMBER_OF_BINS_V," scaling bins, b = ",b, "current scale = ",NUMBER_OF_BINS_V - b
			plt.hist(num_list,bins = NUMBER_OF_BINS_V-b, normed = 1, facecolor='g',alpha=(0.75-(b*.02)))
	else:
			plt.hist(num_list,bins = NUMBER_OF_BINS_V, normed = 1, facecolor='g',alpha=(0.75))

	plt.title(title)
	plt.xlabel('Length to Domain Edge')
	plt.ylabel('Number of Distances to Domain Edge')
	plt.grid(True)
	#plt.title('Histogram of IQ')
	filename = inFile1.replace(".txt","")

	fname = dataDir + filename
	plt.savefig(fname) # save the plot

        print "  * Saved as .png file:",fname
        if MAKE_GRAPH == True:
                plt.show() # display

#end of histogram()


def histogram1(inFile1):
# another histogram for plotting lists of numbers from pDomQueries.

        print "  Histograme Bar Chart for lists of numbers."
#        print "  The data must look like the folloinwg:"
#        print "\n  Third colomn data to graph, plotted in chronological order:"
#        print "138"
#        print "92"
#        print "12\n"

        data = openFile(inFile1)
        num_list = []
        title = str(inFile1)


        for line in data:
#remove zeros since they interfere with the distributions.
		if float(line) != 0.0:
			num_list.append(float(line))

        print "  Data title :", title#,type(title)
        #print "num_list : ",num_list

        # make a bar chart
        #plt.hist(num_list,bins=20,normed = 1, facecolor='g',alpha=0.75)

        plt.hist(num_list,bins=NUMBER_OF_BINS,normed = 1, facecolor='g',alpha=0.75)

        plt.title(title)
        plt.xlabel('Length to Domain Edge')
        plt.ylabel('Number of Distances to Domain Edge')
        plt.grid(True)
        #plt.title('Histogram of IQ')
        filename = inFile1.replace(".txt","")
        fname = dataDir + filename
        plt.savefig(fname) # save the plot
        print "  * Saved as .png file:",fname
        if MAKE_GRAPH == True:
                plt.show() # display
#end of histogram1()


def titledBoxPlot(inFile1_str):
#format: the first line has the title of the data.
#AAAnMt_Near-Domain-Start_acetylation
#acetylation	p35241:1	0.744827586207
#acetylation	p35241:0	0.810344827586
#acetylation	p19338:8	0.527027027027
#acetylation	p35241:2	0.537931034483

# ref: http://matplotlib.org/faq/howto_faq.html#how-to-search-examples
# erf: http://matplotlib.org/examples/pylab_examples/boxplot_demo.html
# method to design the box plots
# Loads a file with pdom data in following format:
#Near-Domain-Start_acetylation
#acetylation    p35241:1        0.744827586207
#acetylation    p35241:0        0.810344827586
#	from pylab import *

	from pylab import plot, show, savefig, xlim, figure, hold, ylim, legend, boxplot, setp, axes
	inFile1_str = string.split(inFile1_str)
	data_dic = {} # holds the data for the boxplot. note all these sets are plotted in same plot
	for f in inFile1_str:
		print " Opening data File : ",f
		data_dic[f] = openFile(f)
	#print data_dic

	graphThis_dic = {} # holds the data lists to plot (value) using title (key)
	for d in data_dic:
		num_list = [] # holds the list of nums to plot
		currentSet_list = data_dic[d]
		#print " currentSet_list :",d,type(currentSet_list)
		for line in currentSet_list:
	                sline = string.split(line)
	                try:
	                        num_list.append(float(sline[2]))
	                except:
				title = line
	                        #print "title = ",title
		graphThis_dic[title] = num_list
#	printer(graphThis_dic)
	# add lists to boxData
	print ""
	boxyData_list = [] # holds each box plot
	label_list = [] # keep track of the names of datasets
	count = 1 # to keep track of the data sets and will be used in the legend
	for i in graphThis_dic:
		print "  Preparing boxplot: ",i.replace("\n","")
		addLine = str(count) + " :" + str(i)
		label_list.append(addLine)
		tmp_list = graphThis_dic[i]
		boxyData_list.append(tmp_list)
		count = count + 1
#	print "boxyData_list :",boxyData_list
        boxplot(boxyData_list)

	# draw temporary red and blue lines and use them to create a legend
	hB, = plot([1,1],'b-')
	hR, = plot([1,1],'r-')
	#legend((hB, hR),('Apples', 'Oranges'))

	legendMarker = []
	for i in graphThis_dic:
		legendMarker.append(hB)

	#legend((hB, hR,hB),(label_list))
	legend((legendMarker),(label_list))
	hB.set_visible(False)
	hR.set_visible(False)

        tmp = checkDataDir()
	fname = dataDir
	for f in data_dic:
		fname = fname  + str(f) + "_"

        if len(fname) > 90:
                import time
                c = time.clock()
                c = str(c).replace(".","") # make some time-coded filename if too long otherwise.
                fname = dataDir + "all_" + c + "_"
	fname = fname.replace(".txt","")
        fname = fname + "boxplot.png"
	savefig(fname)
	if MAKE_GRAPH == True:
		show()
        print " * Saved in :", fname
#end of titledBoxPlot()


def untitledBoxPlot(inFile1_str):
# format: note, there is no title on the first line.
#acetylation	q5xic0:0	12
#acetylation	q5xic0:1	23
#acetylation	q5xic0:2	53

# ref: http://matplotlib.org/faq/howto_faq.html#how-to-search-examples
# erf: http://matplotlib.org/examples/pylab_examples/boxplot_demo.html
# method to design the box plots
# Loads a file with pdom data in following format:
#Near-Domain-Start_acetylation
#acetylation    p35241:1        0.744827586207
#acetylation    p35241:0        0.810344827586
#	from pylab import *

	from pylab import plot, show, savefig, xlim, figure, hold, ylim, legend, boxplot, setp, axes
	inFile1_str = string.split(inFile1_str)
	data_dic = {} # holds the data for the boxplot. note all these sets are plotted in same plot
	for f in inFile1_str:
		print " Opening data File : ",f
		data_dic[f] = openFile(f)
	#print data_dic
	title = str(inFile1_str[0])
	graphThis_dic = {} # holds the data lists to plot (value) using title (key)

	for d in data_dic:
		num_list = [] # holds the list of nums to plot
		currentSet_list = data_dic[d]
		#print " currentSet_list :",d,type(currentSet_list)
		title = str(d)
		for line in currentSet_list:
	                sline = string.split(line)
	                try:
	                        num_list.append(float(sline[2]))
	                except:
				pass
		graphThis_dic[title] = num_list
	print " number of sets in graphThis_dic :",len(graphThis_dic)
#	printer(graphThis_dic)
	# add lists to boxData
	print ""
	boxyData_list = [] # holds each box plot
	label_list = [] # keep track of the names of datasets
	count = 1 # to keep track of the data sets and will be used in the legend
	for i in graphThis_dic:
		print "  Preparing boxplot: ",i.replace("\n","")
		addLine = str(count) + " :" + str(i)
		label_list.append(addLine)
		tmp_list = graphThis_dic[i]
		boxyData_list.append(tmp_list)
		count = count + 1
#	print "boxyData_list :",boxyData_list
        boxplot(boxyData_list)

	# draw temporary red and blue lines and use them to create a legend
	hB, = plot([1,1],'b-')
	hR, = plot([1,1],'r-')
	#legend((hB, hR),('Apples', 'Oranges'))

	legendMarker = []
	for i in graphThis_dic:
		legendMarker.append(hB)

	#legend((hB, hR,hB),(label_list))
	legend((legendMarker),(label_list))
	hB.set_visible(False)
	hR.set_visible(False)

        tmp = checkDataDir()
	fname = dataDir
	for f in data_dic:
		fname = fname  + str(f) + "_"

	if len(fname) > 90:
		import time
		c = time.clock()
		c = str(c).replace(".","") # make some time-coded filename if too long otherwise.
		fname = dataDir + "all_" + c + "_"
	fname = fname.replace(".txt","")
        fname = fname + "boxplot.png"
	savefig(fname)
	if MAKE_GRAPH == True:
		show()
        print " * Saved in :", fname

#end of untitledBoxPlot()



def untitledBoxPlot_justALineOfNums(inFile1_str):
# format: note, there is no title on the first line.
#acetylation	q5xic0:0	12
#acetylation	q5xic0:1	23
#acetylation	q5xic0:2	53

# ref: http://matplotlib.org/faq/howto_faq.html#how-to-search-examples
# erf: http://matplotlib.org/examples/pylab_examples/boxplot_demo.html
# method to design the box plots
# Loads a file with pdom data in following format:
#Near-Domain-Start_acetylation
#acetylation    p35241:1        0.744827586207
#acetylation    p35241:0        0.810344827586
#	from pylab import *

	from pylab import plot, show, savefig, xlim, figure, hold, ylim, legend, boxplot, setp, axes
	inFile1_str = string.split(inFile1_str)
	data_dic = {} # holds the data for the boxplot. note all these sets are plotted in same plot
	for f in inFile1_str:
		print " Opening data File : ",f
		data_dic[f] = openFile(f)
	#print data_dic
	title = str(inFile1_str[0])
	graphThis_dic = {} # holds the data lists to plot (value) using title (key)

	for d in data_dic:
		num_list = [] # holds the list of nums to plot
		currentSet_list = data_dic[d]
		#print " currentSet_list :",d,type(currentSet_list)
		title = str(d)
		for line in currentSet_list:
	                sline = string.split(line)
	                try:
	                        num_list.append(float(sline[0]))
	                except:
				pass
		graphThis_dic[title] = num_list
#	print " number of sets in graphThis_dic :",len(graphThis_dic)
#	printer(graphThis_dic)
	# add lists to boxData
	print ""
	boxyData_list = [] # holds each box plot
	label_list = [] # keep track of the names of datasets
	count = 1 # to keep track of the data sets and will be used in the legend
	for i in graphThis_dic:
		print "  Preparing boxplot: ",i.replace("\n","")
		addLine = str(count) + " :" + str(i)
		label_list.append(addLine)
		tmp_list = graphThis_dic[i]
		boxyData_list.append(tmp_list)
		count = count + 1
#	print "boxyData_list :",boxyData_list
        boxplot(boxyData_list)

	# draw temporary red and blue lines and use them to create a legend
	hB, = plot([1,1],'b-')
	hR, = plot([1,1],'r-')
	#legend((hB, hR),('Apples', 'Oranges'))

	legendMarker = []
	for i in graphThis_dic:
		legendMarker.append(hB)

	#legend((hB, hR,hB),(label_list))
	legend((legendMarker),(label_list))
	hB.set_visible(False)
	hR.set_visible(False)

        tmp = checkDataDir()
	fname = dataDir
	for f in data_dic:
		fname = fname  + str(f) + "_"

        if len(fname) > 90:
                import time
                c = time.clock()
                c = str(c).replace(".","") # make some time-coded filename if too long otherwise.
                fname = dataDir + "all_" + c + "_"
	fname = fname.replace(".txt","")
        fname = fname + "boxplot.png"
	savefig(fname)
	if MAKE_GRAPH == True:
		show()
        print " * Saved in :", fname

#end of untitledBoxPlot_justALineOfNums()






def scatterPlot(inFile1):
#http://matplotlib.org/examples/shapes_and_collections/scatter_demo.html

	N = 50
	#x = np.random.rand(N)
	#y = np.random.rand(N)

	data_list = openFile(inFile1)
	data_list  = sorted(data_list)
	x_list = []
	area = []
	for i in data_list:
		num = int(i.replace("\n",""))
		print "i,num = ",i,num,type(i),type(num)
		area = (np.pi * (num*0.15))**2
		num = 1 # put all data at the y=1 line.
		x_list.append(num)
	colors = np.random.rand(N)


	#area = np.pi * (15 * np.random.rand(N))**2 # 0 to 15 point radiuses
	#area =  np.pi * (30 * np.random.rand(N))**2 # 0 to 15 point radiuses
#	area =  np.pi * (25)**2 # 0 to 15 point radiuses

	print " colour coefficient: ",colors

	#plt.scatter(x_list, data_list, s=area, c=colors, alpha=0.75)
	plt.scatter(x_list, data_list, s=area, c='g', alpha=0.75)
	fname = inFile1
	fname = fname.replace(".txt","")
        fname = dataDir + fname + "_scatterPlot.png"
        if MAKE_GRAPH == True:
                plt.show()

        plt.savefig(fname)
        print " * Saved in :", fname

#end of scatterPlot()

def domainsAndPTMsInProtein(inFile1):
	print "\n  domainsAndPTMsInProtein():",inFile1


#       print "  Fancy Green Histograme Bar Chart."
#       print "  The data must look like the folloinwg:"
#acetylation	PTM_1	0.276048714479
#acetylation	dStart_1	0.0727023319616
#acetylation	dEnd_1	0.149519890261



        data = openFile(inFile1)
        num_list = [] # combined lists
	ptmNum_list = [] # green ptm points
	dStartNum_list = [] # red domain start points
	dEndNum_list = [] # red domain end points
        title = ""
        for line in data:
                #print "line =",line
                if "exhaustive" in string.lower(line):
                #if "domain" in string.lower(line):
                        title = line
                else:
                        sline = string.split(line)
                        #print sline
                        try:
				if "ptm" in string.lower(sline[1]): # is a ptm point
#					print "PTM point found: ",sline
					ptmNum_list.append(float(sline[2]))

				if "start" in string.lower(sline[1]): # is a domain start point
#					print "dom start point found: ",sline
					dStartNum_list.append(float(sline[2]))

				if "end" in string.lower(sline[1]): # is a domain end point
#					print "dom end point found: ",sline
					dEndNum_list.append(float(sline[2]))

                        except ValueError:
                                print "sline[2] = ",sline[2], "is not an int..."
        print "  Data title :", title#,type(title)

#combine all lists into one single list.

        # make a bar chart

        if ROLLING_BINS == True: #super impose graphs of smaller bins?
                for b in range(0,NUMBER_OF_BINS_V-1,2):
#                        print "PTMs, c = 1", NUMBER_OF_BINS_V," scaling bins, b = ",b, "current scale = ",NUMBER_OF_BINS_V - b
                        plt.hist(ptmNum_list,bins = NUMBER_OF_BINS_V-b, normed = 1, facecolor='g',alpha=(0.75-(b*.02)))
        else:
			print "c = 1"
                        plt.hist(ptmNum_list,bins = NUMBER_OF_BINS_V, normed = 1, facecolor='g',alpha=(0.75))

        if ROLLING_BINS == True: #super impose graphs of smaller bins?
                for b in range(0,NUMBER_OF_BINS_V-1,2):
#                        print "Dom Start, c = 2 ",NUMBER_OF_BINS_V," scaling bins, b = ",b, "current scale = ",NUMBER_OF_BINS_V - b
                        plt.hist(dStartNum_list,bins = NUMBER_OF_BINS_V-b, normed = 1, facecolor='r',alpha=(0.75-(b*.02)))
        else:
			print "c = 2"
                        plt.hist(dStartNum_list,bins = NUMBER_OF_BINS_V, normed = 1, facecolor='r',alpha=(0.75))
        if ROLLING_BINS == True: #super impose graphs of smaller bins?
                for b in range(0,NUMBER_OF_BINS_V-1,2):
#                        print "Dom End, c = 3 ",NUMBER_OF_BINS_V," scaling bins, b = ",b, "current scale = ",NUMBER_OF_BINS_V - b
                        plt.hist(dEndNum_list,bins = NUMBER_OF_BINS_V-b, normed = 1, facecolor='b',alpha=(0.75-(b*.02)))
        else:
			print "c = 3"
                        plt.hist(dEndNum_list,bins = NUMBER_OF_BINS_V, normed = 1, facecolor='b',alpha=(0.75))


        plt.title(title)
        plt.xlabel('Proportional Length from Protein Start, Green = PTMs, Red, Blue = domain Starts, Ends, resp.')
        plt.ylabel('Number of Distances at this Proportional Distance')
        plt.grid(True)
        #plt.title('Histogram of IQ')
        filename = inFile1.replace(".txt","")

        fname = dataDir + filename
        plt.savefig(fname) # save the plot

        print "  * Saved as .png file:",fname
        if MAKE_GRAPH == True:
                plt.show() # display


#end of domainsAndPTMsInProtein()

def begin(task, inFile1):
#	print "  Welcome to the toGraph program to make networkX graphics."

# the order of arguments is not important
	t = task
	f = inFile1



# hold for now
	if len(t) == 1:
		task = t
	else:
		inFile1 = t
		task = f




#	print "file :",inFile1
#	print "task :", task

 	#plt.figure(num=1, figsize=(5, 30), dpi=80) #for tall figures
 	#plt.figure(num=1, figsize=(15, 12), dpi=80)
 	plt.figure(num=1, figsize=(15, 18), dpi=80)
	fig = plt.figure(1)
# Be sure to install the libraries.
# using sudo apt-get install python-matplotlib

	tmp = checkDataDir()

	if string.lower(task) == "h": # heatmap
		toHeatmap(inFile1)

	elif string.lower(task) == "s": # simple graph nodes
#		print "  Graphing data containing only two nodes (asite file from getPTM3_iii.py)."
		twoNodeGrapher(inFile1)

	elif string.lower(task) == "q": # multiple edge graph (queries)
#	       	print "  Graphing query data having only two nodes."
		query_twoNodeGrapher(inFile1)

	elif string.lower(task) == "m": # multiple edge graph (queries)
#		print "  Graphing a query with three nodes (a clique)."
		query_ThreeNodeGrapher(inFile1)

	elif string.lower(task) == "d": # multiple edge graph
#		print "  Graphing demo."
		graphDemo(inFile1)

	elif string.lower(task) == "b": # multiple edge graph
#		print "  pDombasescan.y data to histogram plot."
		#print "Fancy Green Histogram plot"
		histogram(inFile1)

	elif string.lower(task) == "p":
#		print "  Dpom list of numbers."
		histogram1(inFile1)

	elif string.lower(task) == "x":
#		print "  pdom boxplot. Data has title in first line."
		titledBoxPlot(inFile1)

	elif string.lower(task) == "y":
#		print "  pdom boxplot. Data has no title in first line."
		untitledBoxPlot(inFile1)

	elif string.lower(task) == "c":
#		print "  Dpom Scatter plot."
		scatterPlot(inFile1)

	elif string.lower(task) == "n":
#		print "  Dpom box plot: a file of numbers."
		untitledBoxPlot_justALineOfNums(inFile1)

	elif string.lower(task) == "e":
#		print "  Dpom box plot: aspecific domain and all PTMs."
		domainsAndPTMsInProtein(inFile1)

	else:

		print "  Oh, Holmes, what are you babbling about now?!\n  Unknown command : <<",task,">>"
#end of begin()

import math, os, sys, string, random, re
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
#from pylab import * # for some reason, having this import here upsets the histogram method.

if __name__ == '__main__':
#       if len(sys.argv) == 2: #one option added to command line
#       begin(sys.argv[1])

    if len(sys.argv) == 3:
         begin(sys.argv[1],sys.argv[2])#,sys.argv[3], sys.argv[4]),sys.argv[5])
    else:
         help()
         sys.exit(0)
