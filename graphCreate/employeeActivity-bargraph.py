# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = (
    'Sit at a desk and code all day',
    'Test the work of others',
    'Make art',
    'Work in collaborative groups',
    'Play video games all day',
    'Travel for work'
    )
y_pos = np.arange(len(objects))
performance = [36,37,23,40,10,23]

plt.barh(y_pos, performance, align='center', alpha=0.5, color= ["#c6b78e" ,"#b64d6e"])
plt.yticks(y_pos, objects)
plt.xlabel('Number of Responses')
plt.title('Perceived Computer Science Employee Activity')

plt.show()
