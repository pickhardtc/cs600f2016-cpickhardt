# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('No', 'Yes')
y_pos = np.arange(len(objects))
performance = [56.9,43.1]

plt.barh(y_pos, performance, align='center', alpha=0.5, color= ["#c6b78e" ,"#b64d6e"])
plt.yticks(y_pos, objects)
plt.xlabel('Percentage')
plt.title('College Graduate')

plt.show()
