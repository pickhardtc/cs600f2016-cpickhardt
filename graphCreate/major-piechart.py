# Code taken and manipulated from MatPlotLib
# http://matplotlib.org/1.2.1/examples/pylab_examples/pie_demo.html

from pylab import *
from matplotlib import cm

# make a square figure and axes
figure(1, figsize=(6,6))
ax = axes([0.1, 0.1, 0.8, 0.8])

colors = ["#c6b78e" ,"#b64d6e","#f5deb3", "#793061","#FFFFFF","#5f0000","#999999","#b03060"]


# The slices will be ordered and plotted counter-clockwise.
labels = (
    'Accounting',
    'Anthropology',
    'Applied Math',
    'Biology',
    'Biochemistry',
    'Business',
    'Chemistry',
    'Communication Arts',
    'Computer Science',
    'Economics',
    'Education',
    'English',
    'Environmental Science',
    'Geology',
    'Global Health Studies',
    'Government',
    'History',
    'International Studies',
    'Natural Sciences',
    'Neuroscience',
    'Political Science',
    'Psychology',
    'Sociology',
    'Spanish',
    'Undecided',
    'Film/Television/Emerging Media'
    )
fracs = [1,1,2,7,2,2,1,4,1,2,2,2,2,2,1,2,2,1,3,3,9,1,1,1,1,1]
explode=(0,0,0,0,0,0,0,0,0.3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

pie(fracs, explode=explode, labels=labels,
                autopct='%1.1f%%', colors=colors, shadow=True, startangle=90)
                # The default startangle is 0, which would start
                # the Frogs slice on the x-axis.  With startangle=90,
                # everything is rotated counter-clockwise by 90 degrees,
                # so the plotting starts on the positive y-axis.

title('College Major', bbox={'facecolor':'0.8', 'pad':5})

show()
