# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = (
    'Happy',
    'Scared',
    'Intrigued',
    'Unintelligent',
    'Excited',
    'Intelligent',
    'Indifferent',
    'Confused',
    'Wonderful'
    )
y_pos = np.arange(len(objects))
performance = [3,8,23,14,6,8,11,13,1]

plt.barh(y_pos, performance, align='center', alpha=0.5, color= ["#c6b78e" ,"#b64d6e"])
plt.yticks(y_pos, objects)
plt.xlabel('Number of Individuals')
plt.title('Sentiment Towards Computer Science')

plt.show()
