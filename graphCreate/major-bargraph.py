# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = (
    'Accounting',
    'Anthropology',
    'Applied Math',
    'Biology',
    'Biochemistry',
    'Business',
    'Chemistry',
    'Communication Arts',
    'Computer Science',
    'Economics',
    'Education',
    'English',
    'Environmental Science',
    'Geology',
    'Global Health Studies',
    'Government',
    'History',
    'International Studies',
    'Natural Sciences',
    'Neuroscience',
    'Political Science',
    'Psychology',
    'Sociology',
    'Spanish',
    'Undecided',
    'Film/Television/Emerging Media'
    )
y_pos = np.arange(len(objects))
performance = [1,1,2,7,2,2,1,4,1,2,2,2,2,2,1,2,2,1,3,3,9,1,1,1,1,1]

plt.barh(y_pos, performance, align='center', alpha=0.5, color= ["#c6b78e" ,"#b64d6e"])
plt.yticks(y_pos, objects)
plt.xlabel('Number of Individuals')
plt.title('College Graduate')

plt.show()
