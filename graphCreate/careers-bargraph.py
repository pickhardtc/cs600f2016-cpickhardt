# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = (
    'Programmer/Programming',
    'Engineer/Engineering',
    'Knowledge',
    'Computer(s)',
    'Science',
    'Software',
    'Code/Coding',
    'Design/Designer/Designing',
    'Developer/Development',
    'Technology/Tech',
    'Media & Film',
    'Support',
    'Data/Database',
    'Web/Website',
    'Management',
    'Marketing',
    'Architecture',
    'IT',
    'Information Sys./Informatics',
    'Graphic Arts',
    'Statistics',
    'Games/Gaming',
    'Government',
    'Security',
    'Editor',
    'App',
    'Business',
    'Analyst/Analysis',
    'Creator',
    'Hacking'
    )
y_pos = np.arange(len(objects))
performance = [26,8,2,12,3,12,5,11,8,10,2,2,3,5,1,2,1,7,2,1,1,3,1,4,1,2,1,2,1,1]

plt.barh(y_pos, performance, align='center', alpha=0.5, color= ["#c6b78e" ,"#b64d6e"])
plt.yticks(y_pos, objects)
plt.xlabel('Number of Words')
plt.title('Words used to Describe Career Prospects with CS Knowledge')

plt.show()
