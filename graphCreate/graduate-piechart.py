# Code taken and manipulated from MatPlotLib
# http://matplotlib.org/1.2.1/examples/pylab_examples/pie_demo.html

from pylab import *
from matplotlib import cm

# make a square figure and axes
figure(1, figsize=(6,6))
ax = axes([0.1, 0.1, 0.8, 0.8])

colors = ["#c6b78e" ,"#b64d6e"]


# The slices will be ordered and plotted counter-clockwise.
labels = 'No', 'Yes'
fracs = [56.9,43.1]
explode=(0.1, 0)

pie(fracs, explode=explode, labels=labels,
                autopct='%1.1f%%', colors=colors, shadow=True, startangle=90)
                # The default startangle is 0, which would start
                # the Frogs slice on the x-axis.  With startangle=90,
                # everything is rotated counter-clockwise by 90 degrees,
                # so the plotting starts on the positive y-axis.

title('College Graduate', bbox={'facecolor':'0.8', 'pad':5})

show()
