# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('No','Maybe','Yes')
y_pos = np.arange(len(objects))
performance = [18,13,20]

plt.bar(y_pos, performance, align='center', alpha=0.5, color=["#c6b78e","#999999","#b64d6e"])
plt.xticks(y_pos, objects)
plt.ylabel('Number of Individuals')
plt.title('Would Participants Program in the Future?')

plt.show()
