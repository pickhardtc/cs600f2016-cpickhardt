# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('Males', 'Females')
y_pos = np.arange(len(objects))
performance = [21.6,78.4]

plt.bar(y_pos, performance, align='center', alpha=0.5, color=["#c6b78e" ,"#b64d6e"])
plt.xticks(y_pos, objects)
plt.ylabel('Percentage')
plt.title('Gender')

plt.show()
