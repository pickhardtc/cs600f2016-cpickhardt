# Code taken and manipulated from MatPlotLib
# https://pythonspot.com/en/matplotlib-bar-chart/

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('No','Maybe','Yes')
y_pos = np.arange(len(objects))
performance = [30,1,20]

plt.bar(y_pos, performance, align='center', alpha=0.5, color=["#c6b78e","#999999","#b64d6e"])
plt.xticks(y_pos, objects)
plt.ylabel('Individuals')
plt.title('Programmed Previously')

plt.show()
