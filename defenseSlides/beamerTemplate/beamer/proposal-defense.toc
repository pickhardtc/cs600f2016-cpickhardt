\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{1}{0}{1}
\beamer@sectionintoc {2}{Related Works}{5}{0}{2}
\beamer@sectionintoc {3}{Method of Approach}{8}{0}{3}
\beamer@sectionintoc {4}{Evaluation Strategy}{9}{0}{4}
\beamer@sectionintoc {5}{Preliminary Study}{13}{0}{5}
\beamer@sectionintoc {6}{Research Schedule}{18}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{19}{0}{7}
